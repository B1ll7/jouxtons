<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\JouteurRepository;
use App\Entity\Jouteur;
use App\Form\JouteurType;


class JouteurController extends AbstractController
{
    /**
     * @Route("/jouteur", name="jouteur")
     */
    public function index()
    {
        return $this->render('jouteur/index.html.twig', [
            'controller_name' => 'JouteurController',
        ]);
    }

    /**
    * @Route("/jouteur/jouteur", name="nouveauJouteur")
    */
    public function new(Request $request)
    {
        $entityManager = $this -> getDoctrine()->getManager();
        $message = null;
        $jouteur = new Jouteur();
        $form = $this -> createForm(JouteurType::class, $jouteur, [
          'action' => $this -> generateUrl('nouveauJouteur'),
          'method' => 'POST'
        ]);
        $form->handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid())
        {
            $force = $this -> determinationBonus($_POST['jouteur']['strength'], '');
            $intelligence = $this -> determinationBonus($_POST['jouteur']['intelligence'], '');
            $charisme = $this -> determinationBonus($_POST['jouteur']['charism'], '');
            $experience = $this -> determinationBonus($_POST['jouteur']['experience'], 'exp');
            $jouteur -> setStrength((int)$force)
                    -> setIntelligence((int)$intelligence)
                    ->setCharism((int)$charisme)
                    ->setExperience((int)$experience);
            $entityManager -> persist($jouteur);
            $entityManager->flush();
            $message = $jouteur->getName()." a bien été ajouté à la liste des jouteurs";
            return $this->render('jouteur/newJouteur.html.twig', [
              'message'=> $message,
              'form' => $form -> createView()
            ]);
        }
        return $this->render('jouteur/newJouteur.html.twig', [
            'message' => null,
            'form' => $form -> createView()
        ]);
    }

    /**
    * @Route("/jouteur/determinationBonus", name="determinationBonus")
    */
    public function determinationBonus($value, $value2)
    {
      if(empty($value2))
      {
        switch ($value) {
          case $value >= 201:
            $bonus = 4;
            break;
          case $value >= 101:
            $bonus = 3;
            break;
          case $value >= 51:
            $bonus = 2;
            break;
          default:
            $bonus = 1;
            break;
        }
      }else{
        switch ($value) {
          case $value >= 20:
            $bonus = 4;
            break;
          case $value >= 10:
            $bonus = 3;
            break;
          case $value >= 5:
            $bonus = 2;
            break;
          case $value >= 1:
            $bonus = 1;
            break;
          default:
            $bonus = 0;
            break;
          }
      }
      return $bonus;
    }

    /**
    * @Route("/jouteur/listeJouteur", name="listeJouteur")
    */
    public function show(JouteurRepository $repo)
    {
        $listeJouteur = $repo -> findAll();
        return $this->render('jouteur/listeJouteur.html.twig', [
          'listeJouteur' => $listeJouteur
        ]);
    }
}
