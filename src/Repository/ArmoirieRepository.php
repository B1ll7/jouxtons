<?php

namespace App\Repository;

use App\Entity\Armoirie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Armoirie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Armoirie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Armoirie[]    findAll()
 * @method Armoirie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArmoirieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Armoirie::class);
    }

    // /**
    //  * @return Armoirie[] Returns an array of Armoirie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Armoirie
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
