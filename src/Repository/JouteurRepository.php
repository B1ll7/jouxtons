<?php

namespace App\Repository;

use App\Entity\Jouteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;

/**
 * @method Jouteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jouteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jouteur[]    findAll()
 * @method Jouteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JouteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jouteur::class);
    }

    // /**
    //  * @return Jouteur[] Returns an array of Jouteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Jouteur
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
