<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200518103747 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE armoirie DROP FOREIGN KEY FK_352003D86B3F5C15');
        $this->addSql('DROP INDEX UNIQ_352003D86B3F5C15 ON armoirie');
        $this->addSql('ALTER TABLE armoirie DROP jouteur_id');
        $this->addSql('ALTER TABLE jouteur ADD armoirie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE jouteur ADD CONSTRAINT FK_3C38BD46F598D286 FOREIGN KEY (armoirie_id) REFERENCES armoirie (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3C38BD46F598D286 ON jouteur (armoirie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE armoirie ADD jouteur_id INT NOT NULL');
        $this->addSql('ALTER TABLE armoirie ADD CONSTRAINT FK_352003D86B3F5C15 FOREIGN KEY (jouteur_id) REFERENCES jouteur (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_352003D86B3F5C15 ON armoirie (jouteur_id)');
        $this->addSql('ALTER TABLE jouteur DROP FOREIGN KEY FK_3C38BD46F598D286');
        $this->addSql('DROP INDEX UNIQ_3C38BD46F598D286 ON jouteur');
        $this->addSql('ALTER TABLE jouteur DROP armoirie_id');
    }
}
