<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516225134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jouteur ADD monture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE jouteur ADD CONSTRAINT FK_3C38BD46D40ADBBC FOREIGN KEY (monture_id) REFERENCES monture (id)');
        $this->addSql('CREATE INDEX IDX_3C38BD46D40ADBBC ON jouteur (monture_id)');
        $this->addSql('ALTER TABLE monture DROP FOREIGN KEY FK_3E1B952B6B3F5C15');
        $this->addSql('DROP INDEX IDX_3E1B952B6B3F5C15 ON monture');
        $this->addSql('ALTER TABLE monture DROP jouteur_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jouteur DROP FOREIGN KEY FK_3C38BD46D40ADBBC');
        $this->addSql('DROP INDEX IDX_3C38BD46D40ADBBC ON jouteur');
        $this->addSql('ALTER TABLE jouteur DROP monture_id');
        $this->addSql('ALTER TABLE monture ADD jouteur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monture ADD CONSTRAINT FK_3E1B952B6B3F5C15 FOREIGN KEY (jouteur_id) REFERENCES jouteur (id)');
        $this->addSql('CREATE INDEX IDX_3E1B952B6B3F5C15 ON monture (jouteur_id)');
    }
}
