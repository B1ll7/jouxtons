<?php

namespace App\Form;

use App\Entity\Jouteur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class JouteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name' , null,  [
              'label' => false
            ])
            ->add('strength',  null, [
                'label' => false
            ])
            ->add('intelligence',  null, [
                'label' => false
            ])
            ->add('charism',  null, [
                'label' => false
            ])
            ->add('armoirie', FileType::class, [
                'label' => 'armoirie',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Ajoutez une image au format png ou jpg',
                    ])
                ],
            ])
            // ->add('armoirie', FileType::class, [
            //   'label' => false,
            //   'mapped'=> false,
            //   'required' => false,
            //   'Constraints'=> [
            //     new File([
            //       'maxSize'=>'1024',
            //       'mimeTypes'=>[
            //         'image/png',
            //         'image/jpg'
            //       ],
            //       'mimeTypesMessage'=>'Ajoutez une image au format png ou jpg',
            //     ])
            //   ],
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Jouteur::class,
            'allow_extra_fields'=> true
        ]);
    }
}
