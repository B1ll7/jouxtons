<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JouteurRepository")
 * @ApiResource(
  *     itemOperations={
  *         "get",
  *         "put",
  *         "delete",
  *         "get_weather": {
  *             "method": "GET",
  *             "path": "/jouteur/confirmation",
  *             "controller": JouteurController::class
  *         }
  * }, collectionOperations={"get", "post"})
 */
class Jouteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $strength;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $intelligence;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $charism;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bonus;

    /**
     * @ORM\OneToOne(targetEntity=Armoirie::class, inversedBy="jouteur", cascade={"persist", "remove"})
     */
    private $armoirie;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStrength(): ?int
    {
        return $this->strength;
    }

    public function setStrength(int $strength): self
    {
        $this->strength = $strength;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(int $intelligence): self
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getCharism(): ?int
    {
        return $this->charism;
    }

    public function setCharism(int $charism): self
    {
        $this->charism = $charism;

        return $this;
    }

    public function getExperience(): ?int
    {
        return $this->experience;
    }

    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setBonus(?int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function getArmoirie(): ?Armoirie
    {
        return $this->armoirie;
    }

    public function setArmoirie(?Armoirie $armoirie): self
    {
        $this->armoirie = $armoirie;

        return $this;
    }

}
