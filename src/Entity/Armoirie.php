<?php

namespace App\Entity;

use App\Repository\ArmoirieRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArmoirieRepository::class)
 */
class Armoirie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Image(
     *      mimeTypes={"image/jpeg", "image/png"},
     *      mimeTypesMessage="Please enter a valid Format (png, jpg)",
     *      maxSize="100k",
     *      maxSizeMessage="Fichier bien trop lourd"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\OneToOne(targetEntity=Jouteur::class, mappedBy="armoirie", cascade={"persist", "remove"})
     */
    private $jouteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getJouteur(): ?Jouteur
    {
        return $this->jouteur;
    }

    public function setJouteur(?Jouteur $jouteur): self
    {
        $this->jouteur = $jouteur;

        // set (or unset) the owning side of the relation if necessary
        $newArmoirie = null === $jouteur ? null : $this;
        if ($jouteur->getArmoirie() !== $newArmoirie) {
            $jouteur->setArmoirie($newArmoirie);
        }

        return $this;
    }

}
