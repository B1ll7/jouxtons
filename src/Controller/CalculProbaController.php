<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\IntelController;
use App\Controller\PuissanceCoupController;

class CalculProbaController extends AbstractController
{
    /**
     * @Route("/calcul/proba", name="calcul_proba")
     */
    public function index()
    {
        return false;
    }

    private function calculDesProba($couple){
      $puissance = new PuissanceCoupController();
      $visee = new PuissanceCoupController();
      if(!empty($visee->tenueVisee($couple[0] -> getIntelligence()))){
          $puissance1 = $puissance->puissanceCoup($couple[0] -> getStrength(), [$couple[1]->getStrength(), $couple[1]->getCharism()]);
          $puissance1[] = $couple[0]->getName();
      }else{
        $puissance1 = ["rate son coups", null, $couple[0]->getName()];
      }
      if(!empty($visee->tenueVisee($couple[1] -> getIntelligence()))){
          $puissance2 = $puissance->puissanceCoup($couple[1] -> getStrength(), [$couple[0]->getStrength(), $couple[0]->getCharism()]);
          $puissance2[] = $couple[1]->getName();
      }else{
        $puissance2 = ["rate son coups", null, $couple[1]->getName()];
      }
        $result = [$puissance1, $puissance2];
        return $result;
    }
}
