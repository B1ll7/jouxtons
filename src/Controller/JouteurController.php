<?php

namespace App\Controller;

use App\Entity\Jouteur;

use App\Form\JouteurType;
use App\Form\ConfirmType;

use App\Controller\ArmoirieController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

use Doctrine\ORM\EntityManagerInterface;

use App\Repository\JouteurRepository;
use App\Repository\MontureRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class JouteurController extends AbstractController
{
    /**
    * @Route("/jouteur", name="jouteur")
    */
    public function index(Request $request, MontureRepository $repo, SluggerInterface $slugger)
    {
        $entityManager = $this -> getDoctrine()->getManager();
        $directory = $this->getParameter('upload_directory');
        $message = null;

        $jouteur = new Jouteur();
        $form = $this -> createForm(JouteurType::class, $jouteur, [
          'action' => $this -> generateUrl('jouteur'),
          'method' => 'POST'
        ]);
        $form->handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid())
        {
            $file = $form->get('armoirie')->getData();
            $force = $this -> determinationBonus($_POST['jouteur']['strength'], '');
            $intelligence = $this -> determinationBonus($_POST['jouteur']['intelligence'], '');
            $charisme = $this -> determinationBonus($_POST['jouteur']['charism'], '');
            $bonus = $this -> determinationBonus($_POST['jouteur']['experience'], 'exp') + isset($_POST['monture']);
            $jouteur -> setStrength((int)$force)
                    -> setIntelligence((int)$intelligence)
                    ->setCharism((int)$charisme)
                    ->setBonus((int)$bonus);
            $armoirie = new ArmoirieController();
            $values = $armoirie->upload($file, $jouteur, $directory);
            foreach ($values as $element) {
              $entityManager -> persist($element);
            }
            $entityManager->flush();
            $message = $jouteur->getName()." a bien été ajouté à la liste des jouteurs";
            return $this->render('jouteur/index.html.twig', [
              'message'=> $message,
              'form' => $form -> createView(),
              'monture' => $repo->findAll()
            ]);
        }
        return $this->render('jouteur/index.html.twig', [
            'message' => null,
            'form' => $form -> createView(),
            'monture' => $repo->findAll()
        ]);
    }
    /**
     * @Route("/jouteur/confirmation/{id}", name="confirmation_new", methods={"POST"})
     * @Route("/jouteur/confirmation", name="confirmation", methods={"GET"})
     */
    public function confirm(Request $request, JouteurRepository $repo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entities = $repo->findAll();
        $data = $request->getContent();
        if (isset($_POST) && $_POST != null) {
          var_dump($_POST);
          die();
        }
        $data = json_decode($data, true);
        if (isset($data) && $data != null){
          $uJouteur = $data['jouteur'];
          $id=(int)$uJouteur['id'];
          $jouteur = $entityManager->getRepository(Jouteur::class)->find($id);

          echo "ça marche";
          die();
           $jouteur ->setName($uJouteur['name'])
                           ->setStrength($uJouteur['strength'])
                           ->setIntelligence($uJouteur['intelligence'])
                           ->setStrength($uJouteur['charism'])
                           ->setBonus('');
            var_dump($jouteur);
            return $this->json([
              'code'=> 200,
              'message'=> 'ça marche',
              'data'=>$jouteur
            ], 200);
           $entityManager->flush();
        }

        $form = $this->createForm(ConfirmType::class, null, [
            'action' => $this -> generateUrl('confirmation'),
            'method' => 'POST',
        ]);

        return $this->render('jouteur/confirmation.html.twig', [
            'jouteurs'=>$repo->findAll(),
            'form' => $form -> createView()
        ]);
    }

    /**
     * @Route("/jouteur/Supprimer/{id}", name="supprimerJouteur")
     */

    public function supprimerJouteur($id, JouteurRepository $repo)
    {
      $entityManager = $this -> getDoctrine()->getManager();
      $entity = $this->getDoctrine()->getRepository(Jouteur::class)->find($id);
      $name = $entity->getName();
      $entityManager->remove($entity);
      $entityManager->flush();
      $message = "$name a bien été supprimé";

      return $this -> show($repo, $message);
    }

    /**
    * @Route("/jouteur/determinationBonus", name="determinationBonus")
    */
    public function determinationBonus($value, $value2)
    {
      if(empty($value2))
      {
        switch ($value) {
          case $value >= 201:
            $bonus = 4;
            break;
          case $value >= 101:
            $bonus = 3;
            break;
          case $value >= 51:
            $bonus = 2;
            break;
          default:
            $bonus = 1;
            break;
        }
      }else{
        switch ($value) {
          case $value >= 20:
            $bonus = 4;
            break;
          case $value >= 10:
            $bonus = 3;
            break;
          case $value >= 5:
            $bonus = 2;
            break;
          case $value >= 1:
            $bonus = 1;
            break;
          default:
            $bonus = 0;
            break;
          }
      }
      return $bonus;
    }

    /**
    * @Route("/jouteur/listeJouteur", name="listeJouteur")
    */
    public function show(JouteurRepository $repo, $value=null)
    {
        $listeJouteur = $repo -> findAll();
        return $this->render('jouteur/listeJouteur.html.twig', [
          'listeJouteur' => $listeJouteur,
          'message'=>$value
        ]);
    }
}
