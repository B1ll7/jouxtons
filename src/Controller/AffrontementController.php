<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\JouteurRepository;
use App\Form\AffrontementType;
use App\Form\JouteurType;
use App\Entity\Jouteur;
use App\Controller\CalculProbaController;

class AffrontementController extends AbstractController
{
    /**
     * @Route("/affrontement", name="affrontement")
     */
    public function index(JouteurRepository $repo, Request $request){
      $data = $request->getContent();
      $data = json_decode($data, true);
      if (isset($data) && $data != null){
          $content =$data;
          $result = $this->resolutionCombat($content);
          return $this->json([
            'code'=> 200,
            'message'=> 'ça marche',
            'form'=>$data['numberForm'],
            'data'=>$result
          ], 200);
      }
      $selection = new SelectionPouleController();
      $values = $selection->random($repo->findAll());
      return $this->render('affrontement/index.html.twig', [
        'poules'=>$values
      ]);
    }

    private function resolutionCombat($data){
        $value = [$data['jouteur']['name'], $data['jouteur']['strength'], $data['jouteur']['intelligence'], $data['jouteur']['charism']];
        $value2 = [$data['jouteur2']['name'], $data['jouteur2']['strength'], $data['jouteur2']['intelligence'], $data['jouteur2']['charism']];
        $jouteur1 = $this->initalizerJouteur($value);
        $jouteur2 = $this->initalizerJouteur($value2);
        $couple = [$jouteur1, $jouteur2];
        $proba = new CalculProbaController();
        $resultCombat = $proba->calculDesProba($couple);
        return $resultCombat;
    }

    /**
    * Initialise les jouteurs et renvoie le jouteur avec les valeurs entrée
    */
    private function initalizerJouteur($value){
        $jouteur = new Jouteur();
         $jouteur -> setName($value[0])
                -> setStrength((int)$value[1])
                -> setIntelligence((int)$value[2])
                -> setCharism((int)$value[3]);
        return $jouteur;
    }

}
