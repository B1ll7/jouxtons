<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlessureController extends AbstractController
{
    /**
     * @Route("/blessure", name="blessure")
     */
    public function index()
    {
        return false;
    }

    /**
    * Calcule la blessure en fonction de puissance coup, dans le cas de duel à cheval ou duel à l'épée
    */
    private function blessure($value, $value2){
      $resistance = rand(1,12) + $value2;
      $blessure = (int)$value - (int)$resistance;
      switch($blessure){
        case $blessure >= 14:
          return [3,"blessure grave de l'adversaire"];
          break;
        case $blessure >= 11:
          return [2,"blessure moyenne de l'adversaire"];
          break;
        case $blessure >= 8:
          return [1,"blessure legere de l'adversaire"];
          break;
        default:
          return [0,"adversaire indemne"];
          break;
      }
    }
}
