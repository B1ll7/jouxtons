<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\IntelController;
use App\Controller\BlessureController;

class PuissanceCoupController extends AbstractController
{
    /**
     * @Route("/puissance/coup", name="puissance_coup")
     */
    public function index()
    {
        return false;
    }

    /**
    * Calcule puissance du coups, lance calcul blessure, renvoie 1 en cas de bris ou 0 dans le cas de non bris de lance
    */
    private function puissanceCoup($value, $value2){
      $blessure = new BlessureController;
      $tenueEnSelle = new IntelController;

        $puissance = rand(1,12) + $value;
        switch ($puissance) {
          case $puissance >= 12:
            $message = 'Bris de lance';
            $blessure = $blessure -> blessure($puissance, $value2[0]);
            return [$message, $blessure];
            break;
          case $puissance >= 6:
            $message = 'Bris de lance';
            $result = [$tenueEnSelle -> tenueSelle($value2[1]), $tenueEnSelle->blessure($puissance, $value2[0])];
            return [$message, $result];
            break;
          default:
            $message = 'Pas de bris de lance';
            $tenueEnSelle = $tenueEnSelle->tenueSelle($value2[1]);
            return [$message, $tenueEnSelle];
            break;
        }
    }
}
