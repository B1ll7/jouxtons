<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\JouteurRepository;
use App\Entity\Jouteur;

class SelectionPouleController extends AbstractController
{
    /**
     *
     */
    public function random($listeJouteur)
    {
       $selection = $this->selection($listeJouteur);

       shuffle($listeJouteur);
       for($compt = 0; $compt < $selection; $compt++)
       {
         if (count($listeJouteur) % 2 == 0 && count($listeJouteur) > 2) {
           $values = array_rand($listeJouteur, 2);
           $jouteursChoisi[] = [$listeJouteur[$values[0]], $listeJouteur[$values[1]]];
           unset($listeJouteur[$values[0]]);
           unset($listeJouteur[$values[1]]);

         } else if (count($listeJouteur) == 2) {
           shuffle($listeJouteur);
           $jouteursChoisi[] = [$listeJouteur[0], $listeJouteur[1]];
         } else {
           $values = array_rand($listeJouteur, 1);
           $jouteursChoisi[] = $listeJouteur[$values];
           unset($listeJouteur[$values]);
         }
       }

       return $jouteursChoisi;
    }

    private function selection($listeJouteur){
      if (count($listeJouteur) % 2 == 0) {
         return count($listeJouteur)/2;
      } else {
         return (int)ceil(count($listeJouteur)/2);
      }
    }
}
