<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Armoirie;
use App\Entity\Jouteur;

class ArmoirieController extends AbstractController
{
    /**
     * @Route("/armoirie", name="armoirie")
     */
    public function index()
    {
        return $this->render('armoirie/index.html.twig', [
            'controller_name' => 'ArmoirieController',
        ]);
    }

    public function upload($file, Jouteur $jouteur, $directory)
    {
            if (isset($file) && $file != null) {
              $image = new Armoirie();
              $targetRepository = "image\\";
              $fileName = md5(uniqid()) . '.' . $file->guessExtension();
              try {
                $file->move($directory, $fileName);
              } catch (FileException $e) {
                  echo 'Error dans l\'archivage de l\'image contactez un dev';
              }
              $image->setName($fileName)
                  ->setPath($targetRepository . $image->getName())
                  ->setJouteur($jouteur);
              $jouteur->setArmoirie($image);
          return [$jouteur,$image];
      } else {
          $image = new Armoirie();
          $targetRepository = "image\\";
          // fait la persistance et le flush dans le cas ou l'image est nulle
          $image -> setName('default.jpg')
                  -> setPath($targetRepository.$image->getName())
                  ->setJouteur($jouteur);
          $jouteur -> setArmoirie($image);
          return [$jouteur, $image];
      }
    }
}
