<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IntelController extends AbstractController
{
    /**
     * @Route("/intel", name="intel")
     */
    public function index()
    {
        return $this->render('intel/index.html.twig', [
            'controller_name' => 'IntelController',
        ]);
    }

    /**
    * calcule la tenue en selle, ou la visee, renvoie 1 si jet reussi rien HaruDestination
    */
    private function tenueVisee($value){
        $result = rand(1,12) + $value;
        if($result >= 10){
          return [1, "touché"];
        }
        return [0, "rate"];
    }
    /**
    * calcule la tenue en selle, renvoie 1 si jet reussi rien HaruDestination
    */
    private function tenueSelle($value){
        $result = rand(1,12) + $value;
        if($result >= 10){
          return [1, "maintien sur selle de l'adversaire"];
        }
        return [0, "chute de l'adversaire"];
    }
}
